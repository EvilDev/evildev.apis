﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using EvilDev.Apis.Github.Model;
using Newtonsoft.Json;

namespace EvilDev.Apis.Github
{
    public class GithubClient
    {
        private readonly string _baseUrl;
        private readonly HttpClient _client;

        public GithubClient(string baseUrl)
        {
            _baseUrl = baseUrl;
            _client = new HttpClient
            {
                BaseAddress = new Uri(_baseUrl)
            };

            _client.DefaultRequestHeaders.Add("user-agent", "TheEvilDev");
        }

        public GithubClient() : this(Constants.DefaultUri)
        {
        }

        public async Task<IEnumerable<Repository>> GetUserRepositoriesAsync(string user)
        {
            var message = await _client.GetAsync(string.Format("/users/{0}/repos", user));

            var result = JsonConvert.DeserializeObject<Repository[]>(await message.Content.ReadAsStringAsync());

            return result;
        } 
    }
}
