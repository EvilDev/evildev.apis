﻿using System.Resources;
using System.Reflection;

[assembly: AssemblyTitle("EvilDev.Apis.Github")]
[assembly: AssemblyDescription("Lightweight client wrapper for Github api")]
[assembly: AssemblyCompany("Deville Solutions")]
[assembly: AssemblyCopyright("Copyright © Deville Solutions 2013")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
