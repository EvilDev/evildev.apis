﻿using Newtonsoft.Json;

namespace EvilDev.Apis.Github.Model
{
    public class User
    {
        public long Id { get; set; }
        public string Login { get; set; }

        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; set; }

        [JsonProperty("gravatar_id")]
        public string GravatarId { get; set; }
        public string Url { get; set; }
    }
}