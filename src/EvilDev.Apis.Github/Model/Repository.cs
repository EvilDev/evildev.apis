﻿using System;
using Newtonsoft.Json;

namespace EvilDev.Apis.Github.Model
{
    public class Repository
    {
        public long Id { get; set; }

        public User Owner { get; set; }

        public string Name { get; set; }

        [JsonProperty("full_name")]
        public string FullName { get; set; }
        public string Description { get; set; }

        [JsonProperty("private")]
        public bool IsPrivate { get; set; }

        [JsonProperty("fork")]
        public bool IsFork { get; set; }
        public string Url { get; set; }

        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }

        [JsonProperty("clone_url")]
        public string CloneUrl { get; set; }

        [JsonProperty("git_url")]
        public string GitUrl { get; set; }

        [JsonProperty("ssh_url")]
        public string SSHUrl { get; set; }

        [JsonProperty("svn_url")]
        public string SVNUrl { get; set; }

        [JsonProperty("mirror_url")]
        public string MirrorUrl { get; set; }
        public string Homepage { get; set; }
        public string Language { get; set; }
        public int Forks { get; set; }
        public long Watchers { get; set; }
        public long Size { get; set; }

        [JsonProperty("master_branch")]
        public string MasterBranch { get; set; }

        [JsonProperty("open_issues")]
        public int OpenIssues { get; set; }

        [JsonProperty("pushed_at")]
        public DateTime PushedAt { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}