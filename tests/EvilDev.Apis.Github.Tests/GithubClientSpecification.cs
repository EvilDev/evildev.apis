﻿using System.Linq;
using SharpTestsEx;
using Xunit;

namespace EvilDev.Apis.Github.Tests
{
    public class GithubClientSpecification
    {
        [Fact]
        public void Can_retrieve_user_repositories()
        {
            var sut = new GithubClient();
            var task = sut.GetUserRepositoriesAsync("TheEvilDev");

            task.Wait();
            task.Result.Count().Should().Not.Be(0);
        }
    }
}
